import { ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {
  constructor(private route: ActivatedRoute) { }

  activeTab = 0;

  ngOnInit() {
    this.activeTab = this.route.snapshot.queryParams['tabNum']; 

  }
}
