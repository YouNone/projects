import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.scss']
})
export class ShowComponent implements OnInit {
  constructor(
    private router: Router, 
    private elRef: ElementRef,
    private route: ActivatedRoute
  ) { }

  tabNum: string;
  tabText: string;

  ngOnInit() {   
    console.log(this.route);  
    this.tabNum = this.route.snapshot.queryParams['tabNum'];
    this.tabText = this.route.snapshot.queryParams['dataText']; 
    if (this.route.snapshot.queryParams['dataText'] == undefined) {
      this.tabText = "Brah.."
    }  
    

  }

}
