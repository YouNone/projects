import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ShowComponent } from './show/show.component';
import { TabsComponent } from './tabs/tabs.component'


const appRoutes: Routes = [
  {path: '', component: TabsComponent},
  {path: 'show', component: ShowComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
