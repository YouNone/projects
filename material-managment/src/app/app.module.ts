import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { UsersService } from './shared/services/user.service';
import { AuthService } from './shared/services/auth.service';
import { MaterailModule } from './material.module';
import { MessageService } from './shared/services/message.service';
import { SystemModule } from './system/system.module';
import { BaseApi } from './shared/core/base.api';

const modules = [
  BrowserModule,
  AppRoutingModule,
  BrowserAnimationsModule,
  AuthModule,
  AppRoutingModule,
  HttpClientModule,   
]

@NgModule({
   schemas: [
      CUSTOM_ELEMENTS_SCHEMA
   ],
   declarations: [
      AppComponent,
   ],
   imports: [
      modules,
      MaterailModule,
      SystemModule
   ],
   exports: [
      modules,
      MaterailModule
   ],
   providers: [
      UsersService,
      BaseApi,
      AuthService,
      MessageService,
      
   ],
   bootstrap: [
      AppComponent
   ],
})
export class AppModule { }
