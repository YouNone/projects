import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { UsersService } from 'src/app/shared/services/user.service';
import { User } from 'src/app/shared/models/user.model';
import { Message } from 'src/app/shared/models/message.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { MessageService } from 'src/app/shared/services/message.service';


@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	constructor(
		private usersService: UsersService,
		public dialog: MatDialog,
		private authService: AuthService,
		private router: Router,
		private route: ActivatedRoute,
		public snackBar: MatSnackBar,
		private MSG: MessageService
	) { }

	// SnackBar
	openSnackBar() {
		this.snackBar.openFromComponent(SnackBar, {
			duration: 3000,
		});
	}

	minLength = 3;
	login = new FormControl('', [Validators.required, Validators.minLength(this.minLength)]);
	password = new FormControl('', [Validators.required, Validators.minLength(this.minLength)]);

	loginInForm: FormGroup = new FormGroup({
		login: this.login,
		password: this.password
	})

	message: Message;

	getErrMes(field) {
		return this.loginInForm.get(field).hasError('required') ? this.MSG.getMsg("eEmptyInput") : '';
	}

	ngOnInit() {}

	onSubmit() {
		const formData = this.loginInForm.value;
		this.usersService.getByLogin(formData.login)
			.subscribe((user: User) => {
				
				// console.log(user);
				if (user) {
					if (user.password === formData.password) {
						this.authService.login(user);
						this.openSnackBar();
						// редирект на мейн пейдж
						this.router.navigate(['/system', 'home']);
					} else {
						// диалог неверного пароля
						this.dialog.open(ErrorDialog, {
							height: '200px',
							width: '200px',
							data: {
								type: "Password error",
								text: "Wrong password"
							}
						});

					}
				} else {
					// диалог несуществующего пользователя
					this.dialog.open(ErrorDialog, {
						height: '200px',
						width: '200px',
						data: {
							type: "Login error",
							text: "Login does`t exist"
						}
					});
				}

			})
	}
}
// dialog
@Component({
	selector: 'dialog-data-example-dialog',
	templateUrl: 'dialog.html'
})

export class ErrorDialog {
	constructor(@Inject(MAT_DIALOG_DATA) public data: Message) { }
}
// SnackBar
@Component({
	selector: 'snack-bar.component',
	templateUrl: 'snack-bar.component.html',
	styles: [`
    .hotpink {
      color: hotpink;
    }
  `],
})
export class SnackBar { }