import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { UsersService } from 'src/app/shared/services/user.service';
import { User } from 'src/app/shared/models/user.model';
import { MessageService } from 'src/app/shared/services/message.service';

@Component({
	selector: 'app-registration',
	templateUrl: './registration.component.html',
	styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
	constructor(
		private usersService: UsersService,
		private router: Router,
		private MSG: MessageService
	) { }

	frm: FormGroup;

	getErrMes(field) {
		const inpErrors = this.frm.controls[field].errors;
		for(const key in inpErrors) {
			if (inpErrors.hasOwnProperty(key)) {
				// Показываем ключ ошибки только для email
				const element = inpErrors[key];
				if(field === "email") console.log(field, key, element);

				//  Возпращаем сообщение об ощибке
				let errKey = "";
				switch(key){
					case "required": errKey = "eEmptyInput"; break;
					case "minlength": errKey = "eShortUserName"; break;
					case "email": errKey = "eWrongEmailFormat"; break;
					case "forbidenEmail": errKey = "eExistingEmail"; break;
					default: errKey = "eWorngMsgKey"; break;
				}
				return this.MSG.getMsg(errKey);
			}
		}
	}

	checkControl(name: string): boolean {
		return this.frm.controls[name].invalid;
	}

	ngOnInit() {
		this.frm = new FormGroup({
			fullName: new FormControl(null, [Validators.required, Validators.minLength(3)]),
			login: new FormControl(null, [Validators.required, Validators.minLength(5)]),
			// email: new FormControl(null, [Validators.required, Validators.email]),
			email: new FormControl(null, [Validators.required, Validators.email], this.UsedEmails.bind(this)),
			password: new FormControl(null, [Validators.required, Validators.minLength(3)]),

		});
		// console.log(this.frm);
	}

	onSubmit() {
		console.log("submition!");
		// console.log(this.frm);

		const { password, login, email, fullName } = this.frm.value; // деструктуризация
		const user = new User(password, login, email, fullName)

		this.usersService.createAdmin(user)
			.subscribe((user: User) => {
				this.router.navigate(['/login'])
			});
	}

	UsedEmails(control: FormControl): Promise<any> {
		return new Promise((resolve, reject) => {
			this.usersService.getAdminByEmail(control.value)
				.subscribe(
					(user: User[]) => {
						if (user.length) {
							console.log("Err!");
							resolve({
								forbidenEmail: true
							});
						} else {
							console.log("Ok!");
							resolve(null);
						}
					}
				);
		});
	}

	OnKeyUpEm(event: KeyboardEvent) {
		console.log(this.frm.controls);		
	}
}



	// OnKeyUpEm(event: Event) { // :keyup 
	// 	let emailErr = "";
	// 	let regexp = new RegExp(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/);
	// 	console.log(event);
	// 	this.emailListener.map(
	// 		delay(3000),
	// 		(event) => {
	// 			if (event.value.test(regexp)) {
	// 				this.usersService.getAdminByEmail(event.value).subscribe((user: User[]) => {
	// 					if (user.length === 0) {
	// 						this.frm.controls.email.setValue(true); // нельзя переписать
	// 					} else {
	// 						emailErr = "Current Email is already in use";
	// 					}
	// 				});
	// 			} else {
	// 				emailErr = "Wrong email";
	// 			}
	// 		}
	// 	)
	// }