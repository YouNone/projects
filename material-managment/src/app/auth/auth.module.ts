import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";


import { AuthComponent } from "./auth.component";
import { LoginComponent, ErrorDialog, SnackBar } from "./login/login.component";
import { RegistrationComponent } from "./registration/registration.component";
import { AuthRoutingModule } from "./auth-routing.module";
import { SharedModule } from "../shared/shared.module";
import { MaterailModule } from "../material.module";
// import { SnackBarComponent } from './snack-bar/snack-bar.component';

@NgModule({
	declarations: [
		LoginComponent,
		RegistrationComponent,
		AuthComponent,
		ErrorDialog,
		SnackBar
	],
	imports: [
		CommonModule, // ngIf..
		AuthRoutingModule,
		SharedModule,
		MaterailModule
	],
	entryComponents: [ ErrorDialog, SnackBar ]
})
export class AuthModule {}