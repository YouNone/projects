import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class BaseApi {
	private baseUrl = 'http://localhost:3000/';
	private header: HttpHeaders = new HttpHeaders({});

	constructor(
		public http: HttpClient,
	) {
	}

	public setToken(token: string){
		this.header = new HttpHeaders({token: token});
	}

	private getUrl(url: string = ''): string {
		return this.baseUrl + url;
	}

	public get<T>(url: string = ''): Observable<T> {
		return this.http.get<T>(this.getUrl(url), {headers: this.header});
	}

	public post<T>(url: string = '', data: T): Observable<T> {
		return this.http.post<T>(this.getUrl(url), data, {headers: this.header});
	}

	public put<T>(url: string = '', data: T): Observable<T> {
		return this.http.put<T>(this.getUrl(url), data, {headers: this.header});
	}

	public delete<T>(url: string = '', id: number): Observable<T> {
		return this.http.delete<T>(`${this.getUrl(url)}/${id}`, {headers: this.header});
	}
 
}
