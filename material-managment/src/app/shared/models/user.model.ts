export interface IUser {
	password: string,
	login: string,
	email: string,
	fullName: string,
	id?: number,
	token?: string,
	img?: string,
}

export class User implements IUser{
	constructor(
		public password: string,
		public login: string,
		public email: string,
		public fullName: string,
		public id?: number,
		public token?: string,
		public img?: string,
	) {}
}

export interface IUserDialogData{
	title: string,
	autoFocus: Boolean,
	user: IUser
}