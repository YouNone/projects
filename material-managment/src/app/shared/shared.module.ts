import { NgModule } from "@angular/core";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

const  mod = [
  InfiniteScrollModule,
  FormsModule,
  ReactiveFormsModule
]

@NgModule({
	imports: [mod],
	exports: [mod]
})
export class SharedModule {}