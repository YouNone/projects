import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map } from 'rxjs/operators';
import { Observable } from "rxjs";

import { User } from "../models/user.model";
import { BaseApi } from "../core/base.api";
import { elementEventFullName } from "@angular/core/src/view";
import { AuthService } from "./auth.service";


@Injectable()
export class UsersService{


	constructor(
		private http: BaseApi,
	) {	
	}

	getByLogin(login: string): Observable<User> {
		return this.http.get<User[]>(`auth?login=${login}`)
			.pipe(
				map( (user: User[]) => user[0] ? user[0] : undefined)				
			);
	}

	getAdminByEmail(email: string): Observable<User[]> {
		return this.http.get(`auth?email=${email}`);
	}

	createAdmin(user: User): Observable<User> {
		return this.http.post("auth", user);
	}

	deleteAdmin(id: number): Observable<User> {
        return this.http.delete("auth", id);
	}

	updateAdmin(user: User) {
        return this.http.put(`auth/${user.id}`, user);
	}
	
	// authUser(login: string, password: string): Observable<IauthData> {
	// 	return	this.post<IauthData>(`authUser`, {login, password});
	// }
	
}
