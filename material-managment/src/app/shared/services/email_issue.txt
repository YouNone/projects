// problem
Good day, I had a problem with validating existing emails, 
the cheking works, but when I change emails, it does not go out of error state,

// Validation form
ngOnInit() {
		this.frm = new FormGroup({
			fullName: new FormControl(null, [Validators.required, Validators.minLength(3)]),
			login: new FormControl(null, [Validators.required, Validators.minLength(5)]),
			email: new FormControl(null, [Validators.required, Validators.email, this.UsedEmails.bind(this)]),
			password: new FormControl(null, [Validators.required, Validators.minLength(3)]),

		});
		// console.log(this.frm);
	}
// cheking for existing emails
UsedEmails(control: FormControl): Promise<any> {
		return new Promise((resolve, reject) => {
			this.usersService.getAdminByEmail(control.value)
				.subscribe(
					(user: User[]) => {
						if (user.length) {
							// does not change state when changing email, allways stays in 
							// resolve({forbidenEmail: true});
							console.log("Err!");
							resolve({
								forbidenEmail: true
							});
						} else {
							// do not reach
							console.log("Ok!");
							resolve(null);
						}
					}
				);
		});
	}
// usersService

constructor(
		private http: BaseApi,
	) {	
getAdminByEmail(email: string): Observable<User[]> {
		return this.http.get(`auth?email=${email}`);
	}	

// html form

	<form [formGroup]="frm" (ngSubmit)="onSubmit()">
		<mat-form-field>
			<input 
				matInput 
				placeholder="Enter your full name" 
				formControlName="fullName"
				required>
			<mat-error *ngIf="checkControl('fullName')">{{getErrMes("fullName")}}</mat-error>
		</mat-form-field>
		<mat-form-field>
			<input 
				matInput 
				placeholder="Enter your email" 
				formControlName="email"
				required
				(keyup)="OnKeyUpEm($event)"
			>
			<mat-error *ngIf="checkControl('email')">{{getErrMes("email")}}</mat-error>
		</mat-form-field>
		<mat-form-field>
			<input 
				matInput 
				placeholder="Enter your login" 
				formControlName="login"
				required
			>
			<mat-error *ngIf="checkControl('login')">{{getErrMes("login")}}</mat-error>
		</mat-form-field>
		<mat-form-field>
			<input
				matInput 
				placeholder="Enter your password" 
				formControlName="password" 
				required
				>
			<mat-error *ngIf="checkControl('password')">{{getErrMes("password")}}</mat-error>
		</mat-form-field>
		<hr>
		<button type="submit" id="registBut" mat-raised-button [disabled]="frm.invalid">Submit</button>
	</form>
