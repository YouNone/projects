import { User } from "../models/user.model";
import { BaseApi } from "../core/base.api";
import { Injectable } from "@angular/core";
@Injectable()
export class AuthService {

	constructor(
		private http: BaseApi
	) {}

	private isAuthenticated = false;
	private authData: User;

	login(user: User) {
		this.isAuthenticated = true;
		this.authData = user;
		console.log(this.authData.token);
		
		this.http.setToken(this.authData.token);
		window.localStorage.setItem("user", JSON.stringify(user));
	}

	getUserInfo(): User {
		return this.authData;
	}

	getToken(user: User): string {
		return user.token;
	}

	logout() {
		this.isAuthenticated = false;
		window.localStorage.clear();
		this.authData = null;
		this.http.setToken("");
	}

	isLoggedIn(): boolean {
		return this.isAuthenticated;
	}
}