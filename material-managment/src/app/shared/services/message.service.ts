import { Injectable } from '@angular/core';

@Injectable()
export class MessageService {

	private msg = {
		eWorngMsgKey:					"{WRONG KEY}",
		eShortUserName: 				"Слишком короткий текст.",
		eEmptyInput:					"Поле не может быть пустым.",
		eExistingEmail:					"Текущий Email уже занят",
		eWrongEmailFormat:				"Неверный формат email"
	}

	constructor() { }
	
	getMsg(key: string): string {
		if (this.msg.hasOwnProperty(key)) {
			return this.msg[key];
		} else {
			return this.msg.eWorngMsgKey;
		}
	}
}

