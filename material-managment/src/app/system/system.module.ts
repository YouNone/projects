import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { SharedModule } from "../shared/shared.module";
import { SystemRoutingModule } from "./system-routing.module";
import { HomePageComponent } from "./home-page/home-page.component";
import { UsersListPageComponent } from "./users-list-page/users-list-page.component";
import { SystemComponent } from "./system.component";
import { UserListService } from "./shared/services/userlist.service";
import { HeaderComponent } from "./header/header.component";
import { MenuComponent } from "./menu/menu.component";
import { MaterailModule } from "../material.module";
import { DialogMessageComponent } from "./shared/dialog-components/message-dialog/message-dialog.component";
import { UserDialogComponent } from "./shared/dialog-components/user-dialog/user-dialog.component";
import { BaseApi } from "../shared/core/base.api";
import { AuthModule } from "../auth/auth.module";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		SystemRoutingModule,
		MaterailModule,
		HttpClientModule,
		AuthModule
	],
	declarations: [
		HomePageComponent,
		UsersListPageComponent,
		SystemComponent,
		MenuComponent,
		HeaderComponent,
		DialogMessageComponent,
		UserDialogComponent
	],
	providers: [
		UserListService,
		
	],
	entryComponents: [ 
		DialogMessageComponent, 
		UserDialogComponent 
	]

})
export class SystemModule {}