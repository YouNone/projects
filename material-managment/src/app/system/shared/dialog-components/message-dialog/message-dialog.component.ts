import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Inject, Component } from "@angular/core";


@Component({
	selector: 'message-dialog.component',
	templateUrl: 'message-dialog.component.html',
})

export class DialogMessageComponent {
	static readonly buttons = {
		yes: [
			{
				title: "Ok",
				result: "ok",
			}
		],
		yes_no: [
			{
				title: "Cancel",
				result: "no",
				color: "warn"
			},
			{
				title: "Ok",
				result: "ok",
				color: "primary"
			},
		]
	};


	constructor(
		public dialogRef: MatDialogRef<DialogMessageComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

	// onNoClick(): void {
	// 	this.dialogRef.close();

	// }

	GetResult(str): any {
		return { name: str };
	}
}