import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Inject, Component } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { MessageService } from "src/app/shared/services/message.service";
import { User, IUserDialogData } from "src/app/shared/models/user.model";


@Component({
	selector: 'user-dialog.component',
	templateUrl: 'user-dialog.component.html',
	styleUrls: ['user-dialog.component.scss']

})

export class UserDialogComponent {
	constructor(
		private MSG: MessageService,
		// private router: Router,
		public dialogRef: MatDialogRef<UserDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: IUserDialogData
	) { }

	formDial: FormGroup;
	id: number = undefined;

	checkControl(name: string): boolean {
		return this.formDial.controls[name].invalid;
	}

	getErrMes(field) {
		return this.formDial.get(field).hasError('required') ? this.MSG.getMsg("eEmptyInput") : '';
	}

	ngOnInit() {
		// this.formDial = new FormGroup({
		// 	fullName: new FormControl(null, [Validators.required, Validators.minLength(3)]),
		// 	login: new FormControl(null, [Validators.required, Validators.minLength(5)]),
		// 	email: new FormControl(null, [Validators.required, Validators.email]),
		// 	// email: new FormControl(null, [Validators.required, Validators.email, this.UsedEmails.bind(this)]), ?
		// 	password: new FormControl(null, [Validators.required, Validators.minLength(3)]),
		// });
		// console.log(this.data);
		
		this.formDial = new FormGroup({
			fullName: new FormControl(null, [Validators.required]),
			login: new FormControl(null, [Validators.required]),
			email: new FormControl(null, [Validators.required]),
			password: new FormControl(null, [Validators.required]),
		});
		if (this.data.user.id) {
			this.id = this.data.user.id;
			// set values
			this.formDial.controls.fullName.setValue(this.data.user.fullName);
			this.formDial.controls.login.setValue(this.data.user.login);
			this.formDial.controls.email.setValue(this.data.user.email);
			this.formDial.controls.password.setValue(this.data.user.password);
			console.log(this.formDial);
		}
		
	}

	onCancel(): void {
		this.dialogRef.close();
	}

	
	prepareData(): User{
		const { password, login, email, fullName } = this.formDial.value; // деструктуризация
		return new User(password, login, email, fullName, this.id);
	}
}