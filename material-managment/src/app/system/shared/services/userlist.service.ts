import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

import { User } from "src/app/shared/models/user.model";
import { BaseApi } from "src/app/shared/core/base.api";

@Injectable()
export class UserListService{
	constructor(
		public http: BaseApi
	){
	}

	getList(): Observable<User[]> {
		return this.http.get('user');
	}

	createUser(user: User): Observable<User> {
		return this.http.post("user", user);
	}

	deleteUser(id: number): Observable<User> {
        return this.http.delete("user", id);
	}
	getUserId(id: number): Observable<User> {
		return this.http.get(`user/${id}`);
	}

	updateUser(user: User) {
        return this.http.put(`user/${user.id}`, user);
    }
}
