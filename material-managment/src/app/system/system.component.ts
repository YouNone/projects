import { Component, Input } from "@angular/core";
import { FormControl } from "@angular/forms";

@Component({
	selector: "app",
	templateUrl: './system.component.html',
	styleUrls: ['./system.component.scss']
})
export class SystemComponent  {

	public menuToggle = true;
	public menuMode = 'side';

	stateToggle() {
		this.menuToggle = !this.menuToggle;
	}
}