import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';


import { AuthService } from 'src/app/shared/services/auth.service';
import { DialogMessageComponent } from '../shared/dialog-components/message-dialog/message-dialog.component';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

    dialogResultText: string;

    constructor(
        private authService: AuthService,
        private router: Router,
        public dialog: MatDialog

    ) { }

    openDialog() {
        const dialogRef = this.dialog.open(DialogMessageComponent, {
            data: {
                title: 'Exiting',
                message: "Are you shure want to leave the page?",
                buttons: DialogMessageComponent.buttons.yes_no,
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            this.dialogResultText = result;
            this.dialogResultText
            // console.log( DialogMessageComponent.buttons.yes_no[1].result);
            
            if (Object.values(this.dialogResultText)[0] == DialogMessageComponent.buttons.yes_no[1].result) {
                this.authService.logout();
                this.router.navigate(["/login"]);

            } 
        });
    }

    ngOnInit() {

    }
}
