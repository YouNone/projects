import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { User } from 'src/app/shared/models/user.model';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() clickChange: EventEmitter<boolean> = new EventEmitter();
  
  clickState:boolean;
  user: User;

  constructor(
    // private authService: AuthService,
    // private router: Router,

  ) { }

  public headerToggler() {
    this.clickState = !this.clickState;
    this.clickChange.emit(this.clickState);
    console.log(this.clickState);
    
  }

  ngOnInit() {
    this.user = JSON.parse(window.localStorage.getItem('user'));

  }
}
