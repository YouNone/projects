import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatTable } from '@angular/material';

import { UserListService } from '../shared/services/userlist.service';
import { User } from 'src/app/shared/models/user.model';
import { UserDialogComponent } from '../shared/dialog-components/user-dialog/user-dialog.component';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-users-list-page',
  templateUrl: './users-list-page.component.html',
  styleUrls: ['./users-list-page.component.scss']
})
export class UsersListPageComponent implements OnInit {

  displayedColumns: string[] = [
    'index',
    'id',
    'login',
    'fullName',
    'email',
    'update',
    'delete'
  ];

  data: User[] = [];
  
  dataSource = new MatTableDataSource<User>(this.data);
  resultsLength = 0;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatTable) table: MatTable<User>;

  constructor(
    private userListService: UserListService,
    private authServise: AuthService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.userListService.getList()
      .subscribe((data: User[]) => {
        this.data = data;
        this.dataSource = new MatTableDataSource<User>(this.data);
        this.dataSource.paginator = this.paginator;
        this.resultsLength = data.length;
      });
  }

  // диалог создания/редактирования пользователя
  openDialog(user: User) {
    const dialogRef = this.dialog.open(UserDialogComponent, {
      width: "400px",
      height: "450px",
      data: {
        title: "Dialog",
        autoFocus: true,
        user: Object.assign({}, user)
      }
    });

    dialogRef.afterClosed().subscribe((user: User) => {
      if (user.id) {        
        this.userListService.updateUser(user).subscribe((bdUser: User) => {
          const indx = this.data.findIndex((item: User) => item.id == bdUser.id);
          this.data[indx] = bdUser;
          this.table.renderRows();
          // не обновляется

        });
      } else {
        console.log("createUser", user);
        this.userListService.createUser(user)
          .subscribe((user: User) => {
            // insert into local array
            this.data.push(user);
            this.table.renderRows();
          // не обновляется

          });
          console.log(this.table);

      }
    });

  }

  OnDelete(id: number) {
    this.userListService.deleteUser(id).subscribe(() => {            
      this.data = this.data.filter((item: User) => item.id !== id);

      console.log(this.data);
      // не обновляется
    });
  }
}
