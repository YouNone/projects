import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl} from '@angular/forms';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  
  constructor(
    private router: Router, 
    private route: ActivatedRoute
    ) { }

  dataText: string = "";

  form: FormGroup;
  @Input() tabNum: string = "";

  ngOnInit() {
    this.form = new FormGroup({
      text: new FormControl()
    });    
  }


  onSubmit() {    

    this.dataText = this.form.value.text;  
    
    if (this.dataText == null) {
      this.dataText = "No text sended";
    }
    // console.log("submition!", this.dataText, this.tabNum); 

    this.router.navigate(['/show', this.tabNum, this.dataText]);
  }

  
}