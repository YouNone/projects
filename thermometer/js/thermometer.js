$(function(){
	var MIN = 0,
		MAX = 100,
		destInp = $("[name=target]:checked").val(),
		curFunc = FromCelToFarToKel;
		handle = $("#handle");

	$("#min_val").text(MIN);
	$("#max_val").text(MAX);

	$("#slider").slider({
		min: MIN,
		max: MAX,
		range: "min",
		create: function() {
			handle.text( $( this ).slider( "value" ) );
			},
		slide: function(e, ui){
			handle.text( ui.value );

			// $("#" + destInp).val(ui.value);
			// curFunc(ui.value);

			if (destInp == "inp_celcium") {
				curFunc = FromCelToFarToKel;
				$("#" + destInp).val(ui.value);
				curFunc(ui.value);
			}
			if (destInp =="inp_fahrenheit") {
				curFunc = FromFarToCelToKel;
				$("#" + destInp).val(ui.value);
				curFunc(ui.value);
			}
			if (destInp == "inp_kelvin") {
				curFunc = FromKelToCelToFar;
				$("#" + destInp).val(ui.value);
				curFunc(ui.value);
			}
		}
	});

	$("[name=target]").click(function(){
		destInp = $(this).val();
	});

	function FromCelToFarToKel(cel) {
		var f = Math.round(1.8 * cel + 32); // far
		var k = Math.round(cel + 273); // kel
		
		$("#inp_fahrenheit").val(f);
		$("#inp_kelvin").val(k);
	}
	function FromFarToCelToKel(far) {
		var c = Math.floor((far - 32) * (5 / 9)); // cel
		var k = Math.floor(c + 273);  // kel
	
		$("#inp_celcium").val(c);
		$("#inp_kelvin").val(k);
	}
	function FromKelToCelToFar(kel) {
		var c = Math.floor(kel - 273);  // cel
		var f = Math.floor(1.8 * c + 32) // far

		$("#inp_celcium").val(c);
		$("#inp_fahrenheit").val(f);
	}

});