<?php
/*
Операции:
	get_city
	get_city_list
	get_country_city
	get_country
	get_country_list
	get_continent_country
	get_continent
	get_continent_list
	create_city
	create_country
	create_continent
	delete_city
	delete_country
	delete_continent
	edit_continent
	edit_country
	edit_city
*/


class JsonOut {
	static public function Error($msg){
		//$msg = iconv('cp1251', 'utf-8', $msg);
		echo json_encode(["error" => $msg], JSON_UNESCAPED_UNICODE);
	}
	static public function Out($data){
		echo json_encode($data, JSON_UNESCAPED_UNICODE);
	}
}


class DbSql {
	private $mysqli;
	private $sql = "";
	private $result;
	private $errorState = "";

	private $MSG = [
		"SQL_ERROR"					=> "SQL error: ",
		"SQL_QUERY_NOT_SET"			=> "SQL query not set"
	];

	public function __construct($host, $user, $pass, $db) {
		try{
			$this->mysqli = new mysqli($host, $user, $pass, $db);
			if ($this->mysqli->connect_error) {
				JsonOut::Error("{$this->MSG['SQL_ERROR']} ({$this->mysqli->connect_errno}) {$this->mysqli->connect_error}");
			}
		}catch(Exception $e){
			throw new Exception("{$this->MSG['SQL_ERROR']} ({$this->mysqli->connect_errno}) {$this->mysqli->connect_error}");
		}
	}

	public function SetSql($sql){
		$this->sql = $sql;
	}

	public function ExecQuery($q = ""){
		if($q != "") $this->sql = $q;
		$this->errorState = "";
		if($this->sql == ""){
			$this->errorState = $MSG['SQL_QUERY_NOT_SET'];
			throw new Exception($this->errorState);
		}
		if (!$this->result = $this->mysqli->query($this->sql)) {
			$this->errorState = $this->mysqli->error;
			throw new Exception($this->errorState);
		}
		return $this->result;
	}

	public function GetResultAsArray(){
		if ($this->result->num_rows === 0) {
			return [];
		}
		$list = $this->result->fetch_all(MYSQLI_ASSOC);
		return $list;
	}

	public function GetAffectedRows(){
		return $this->mysqli->affected_rows;
	}

	public function GetCreatedId(){
		return $this->mysqli->insert_id;
	}
}


class World {
	private $HOST = "localhost";
	private $USER = "test";
	private $PASSWORD = "test";
	private $DB = "test";

	private $sql;
	private $currFunc;
	private $params;
	private $MSG = [
		"E_ACTION_NOT_SET"		=> "Action not set",
		"E_BAD_ACTION"			=> "Bad action was set",
		"E_PARAM_NOT_SET"		=> "не задано значение",
		"E_PARAM_BAD_TYPE"		=> "неверный тип",
		"E_BAD_DATA"			=> "Ошибка данных: ",
		"E_EMPTY_PARAM_LIST"	=> "Пустой список параметров",
		"E_ID_NOT_SET"			=> "Не задан корректный ID",
		"E_CREATE_RECORD"		=> "Ошибка создания записи",
		"E_ITEM_NOT_FOUND"		=> "Запись не найдена",
		"E_DUPLICATE_NAME"		=> "Такое имя уже есть: ",
		"E_UPDATE_RECORD"		=> "Ошибка внесения изменений в запись. ",
		"E_DEPENDENCY_PRESENT"	=> "Не могу удалить запись. В подчиненной таблице имеются записи, ссылающиеся на parent_id = "
	];

	public function __construct(){
		// read action and run
		if(isset($_REQUEST['action'])){
			$this->InitSql();
			switch($_REQUEST['action']){
				case "get_city": 			// +
					$this->CheckInput(["id" => "uinteger"]);
					JsonOut::Out( $this->GetItem("city") );
					break;
				case "get_city_list": 		// +
					JsonOut::Out( $this->GetItemsList("city") );
					break;
				case "get_country_city":
					$keys = $this->CheckInput(["id" => "uinteger"]);
					JsonOut::Out( $this->GetItemsList("city", $keys['id']) );
					break;
				case "get_country":			// +
					$this->CheckInput(["id" => "uinteger"]);
					JsonOut::Out( $this->GetItem("country") );
					break;
				case "get_country_list":	// +
					JsonOut::Out( $this->GetItemsList("country") );
					break;
				case "get_continent_country":
					$keys = $this->CheckInput(["id" => "uinteger"]);
					JsonOut::Out( $this->GetItemsList("country", $keys['id']) );
					break;
				case "get_continent":		// +
					$this->CheckInput(["id" => "uinteger"]);
					JsonOut::Out( $this->GetItem("continent") );
					break;
				case "get_continent_list":	// +
					JsonOut::Out( $this->GetItemsList("continent") );
					break;
				case "create_city":
					$keys = $this->CheckInput([
						"name" => "string",
						"parent_id" => "uinteger",
						"population" => "uinteger"
					]);
					if($this->CheckDuplicates("city", $keys['name'])){
						JsonOut::Error($this->MSG["E_DUPLICATE_NAME"]. $keys['name']);
					}else{
						JsonOut::Out( $this->CreateItem("city") );
					}
					break;
				case "create_country":
					$keys = $this->CheckInput([
						"name" => "string",
						"parent_id" => "uinteger",
						//"capital_id" => "uinteger",
						"population" => "uinteger",
						"square"	=> "uinteger"
					]);
					if($this->CheckDuplicates("country", $keys['name'])){
						JsonOut::Error($this->MSG["E_DUPLICATE_NAME"]. $keys['name']);
					}else{
						JsonOut::Out( $this->CreateItem("country") );
					}
					break;
				case "create_continent":
					$keys = $this->CheckInput(["name" => "string"]);
					if($this->CheckDuplicates("continent", $keys['name'])){
						JsonOut::Error($this->MSG["E_DUPLICATE_NAME"]. $keys['name']);
					}else{
						JsonOut::Out( $this->CreateItem("continent") );
					}
					break;
				case "edit_continent":
					$keys = $this->CheckInput([
						"id" => "uinteger",
						"name" => "string"
					]);
					if($this->CheckDuplicates("continent", $keys['name'])){
						JsonOut::Error($this->MSG["E_DUPLICATE_NAME"]. $keys['name']);
					}else{
						JsonOut::Out( $this->UpdateItem("continent") );
					}
					break;
				case "edit_country":
					$keys = $this->CheckInput([
						"id" => "uinteger",
						"name" => "string",
						"parent_id" => "uinteger",
						"capital_id" => "uinteger",
						"population" => "uinteger",
						"square"	=> "uinteger"
					]);
					if($this->CheckDuplicates("country", $keys['name'])){
						JsonOut::Error($this->MSG["E_DUPLICATE_NAME"]. $keys['name']);
					}else{
						JsonOut::Out( $this->UpdateItem("country") );
					}
					break;
				case "edit_city":
					$keys = $this->CheckInput([
						"id" => "uinteger",
						"name" => "string",
						"parent_id" => "uinteger",
						"population" => "uinteger"
					]);
					if($this->CheckDuplicates("city", $keys['name'])){
						JsonOut::Error($this->MSG["E_DUPLICATE_NAME"]. $keys['name']);
					}else{
						JsonOut::Out( $this->UpdateItem("city") );
					}
					break;
				case "delete_city":
					$this->CheckInput(["id" => "uinteger"]);
					JsonOut::Out( $this->DeleteItem("city") );
					break;
				case "delete_country":
					$keys = $this->CheckInput(["id" => "uinteger"]);
					if($this->CheckDependency($keys['id'], "city")){
						JsonOut::Error($this->MSG["E_DEPENDENCY_PRESENT"]. $keys['id']);
					}else{
						JsonOut::Out( $this->DeleteItem("country") );
					}
					break;
				case "delete_continent":
					$keys = $this->CheckInput(["id" => "uinteger"]);
					if($this->CheckDependency($keys['id'], "country")){
						JsonOut::Error($this->MSG["E_DEPENDENCY_PRESENT"]. $keys['id']);
					}else{
						JsonOut::Out( $this->DeleteItem("continent") );
					}
					break;
				default:
					JsonOut::Error("{$this->MSG['E_BAD_ACTION']} {$_REQUEST['action']}");
					exit;
			}
		}else{
			echo JsonOut::Error($this->MSG['E_ACTION_NOT_SET']);
		}

	}

	private function InitSql(){
		try{
			$this->sql = new DbSql($this->HOST, $this->USER, $this->PASSWORD, $this->DB);
		}catch(Exception $e){
			JsonOut::Error($e->getMessage());
			exit;
		}
	}

	public function GetItem($tbl_name, $id = -1){
		if($id < 0 && isset($this->params['id']) && $this->params['id'] > 0){
			$id = $this->params['id'];
		}else if($id < 0 && (!isset($this->params['id']) || $this->params['id'] < 0) ){
			JsonOut::Error($this->MSG['E_ID_NOT_SET']);
			exit;
		}
		try{
			$this->sql->ExecQuery("SELECT * FROM {$tbl_name} WHERE id = '{$id}' LIMIT 1");
			return $this->sql->GetResultAsArray();
		}catch(Exception $e){
			JsonOut::Error($e->getMessage());
			exit;
		}
	}

	public function GetItemsList($tbl_name, $parent_id = -1){
		$addCond = $parent_id > 0 ? "WHERE parent_id = ". $parent_id : "";
		try{
			$this->sql->ExecQuery("SELECT * FROM {$tbl_name} {$addCond} ORDER BY name");
			return $this->sql->GetResultAsArray();
		}catch(Exception $e){
			JsonOut::Error($e->getMessage());
			exit;
		}
	}

	public function CreateItem($tbl_name){
		try{
			if(count($this->params) == 0) throw new Exception($this->MSG['E_EMPTY_PARAM_LIST']);
			$pairs = [];
			foreach($this->params as $key => $value){
				$value = addslashes($value);
				$key = addslashes($key);
				array_push($pairs, "{$key} = '$value'");
			}
			$fields = implode(", ", $pairs);
			$this->sql->ExecQuery("INSERT INTO {$tbl_name} SET {$fields}");
			if($this->sql->GetAffectedRows() == 1){
				return [
					"result" => true,
					"data" => $this->GetItem($tbl_name, $this->sql->GetCreatedId())[0]
				];
			}else{
				JsonOut::Error($this->MSG['E_CREATE_RECORD']);
				exit;
			}
		}catch(Exception $e){
			JsonOut::Error($e->getMessage());
			exit;
		}
	}

	public function UpdateItem($tbl_name){
		try{
			if(count($this->params) == 0) throw new Exception($this->MSG['E_EMPTY_PARAM_LIST']);
			$pairs = [];
			$id = -1;
			foreach($this->params as $key => $value){
				$value = addslashes($value);
				$key = addslashes($key);
				if($key == 'id'){
					$id = $value;
					continue;
				}
				array_push($pairs, "{$key} = '$value'");
			}
			$fields = implode(", ", $pairs);
			$this->sql->ExecQuery("UPDATE {$tbl_name} SET {$fields} WHERE id = '$id' LIMIT 1");
			if($this->sql->GetAffectedRows() == 1){
				return [
					"result" => true,
					"data" => $this->GetItem($tbl_name, $id)[0]
				];
			}else{
				JsonOut::Error($this->MSG['E_UPDATE_RECORD']);
				exit;
			}
		}catch(Exception $e){
			JsonOut::Error($e->getMessage());
			exit;
		}
	}

	public function DeleteItem($tbl_name){
		try{
			$this->sql->ExecQuery("DELETE FROM {$tbl_name} WHERE id = '{$this->params["id"]}'");
			if($this->sql->GetAffectedRows() == 1){
				return ["result" => true, "data" => ["id" => $this->params["id"]]];
			}else if($this->sql->GetAffectedRows() == 0){
				JsonOut::Error($this->MSG['E_ITEM_NOT_FOUND']);
				exit;
			}
		}catch(Exception $e){
			JsonOut::Error($e->getMessage());
			exit;
		}
	}

	// Проверяет есть ли уже в таблице записи с таким же значением поля name
	public function CheckDuplicates($tbl_name, $item_name){
		$this->sql->ExecQuery("SELECT COUNT(*) AS cnt FROM $tbl_name WHERE name = '{$item_name}'");
		$res = $this->sql->GetResultAsArray();
		$f = (int)$res[0]['cnt'] > 1;
		if((int)$res[0]['cnt'] > 1) return true; else return false;
	}

	// Проверяет есть ли в таблице $tbl_name поля с передаваемым parent_id, проверять таблицы перед удаленим записи
	public function CheckDependency($id, $tbl_name){
		$this->sql->ExecQuery("SELECT COUNT(*) AS cnt FROM $tbl_name WHERE parent_id = '$id'");
		$res = $this->sql->GetResultAsArray();
		if((int)$res[0]['cnt'] > 0) return true; else return false;
	}

	private function CheckInput($fieldList){
		$error = false;
		$errors = [];

		$this->params = [];
		foreach($fieldList as $key => $type ){
			if(!isset($_REQUEST[$key])){
				$error = true;
				array_push($errors, "{$this->MSG['E_PARAM_NOT_SET']} $key");
				continue;
			}
			$_REQUEST[$key] = urldecode($_REQUEST[$key]);
			switch($type){
				case "uinteger":
					$this->params[$key] = intval($_REQUEST[$key]);
					if($this->params[$key] <= 0){
						array_push($errors, "{$this->MSG['E_PARAM_BAD_TYPE']} $type {$key} = {$_REQUEST[$key]}");
						$error = true;
					}
					break;
				case "boolean":
					if($_REQUEST[$key] != "true" && $_REQUEST[$key] != "false"){
						array_push($errors, "{$this->MSG['E_PARAM_BAD_TYPE']} $type {$key} = {$_REQUEST[$key]}");
						$error = true;
					}else{
						$this->params[$key] = boolval($_REQUEST[$key]);
					}
					break;
				case "string":
				default:
					$this->params[$key] = addslashes(trim(strip_tags($_REQUEST[$key])));
			}
		}
		if($error){
			array_unshift($errors, $this->MSG['E_BAD_DATA']);
			JsonOut::Error(implode($errors));
			exit;
		}
		return $this->params;
	}

}


//$_REQUEST['action'] = "edit_country";
//$_REQUEST['id'] = "1";
//$_REQUEST['capital_id'] = "1";
//$_REQUEST['parent_id'] = "2";
//$_REQUEST['name'] = "рпарпврпарпврав";
//$_REQUEST['population'] = "100000";
//$_REQUEST['square'] = "146354";


$page = new World();

?>
