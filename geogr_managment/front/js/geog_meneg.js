$(function(){
	// сохраняет локальные данные объекта
	var currData,
		selectedLi;

	// удаляет содержимое value в input
	function ClearInp(tar) {
		$(`${tar} :required`).val("");
	}
	
	// Вызывает диалог с сообщением ошибки и ее типом
	function ShowError(msg){
		$("#dial_message").dialog("option", {title: "Error!"})
		$("#error-msg").text(msg);
		$("#dial_message").dialog("open");
	}
	
	// Вызывает диалог с сообщением о удачной отправке
	function ShowMessage(msg){
		$("#dial_message").dialog("option", {title: "Message"})
		$("#error-msg").text(msg);
		$("#dial_message").dialog("open");
	}
	
	// Заменяет текст выбранного <li> на заданный пользователем
	function ChangeText(obj, selector) {
		var curText = obj.find(".list-name").text($(selector).val());
		//console.log(curText);
		return curText;
	}

	// Проверка заполнения обязательных полей
	// selector -- селектор целевого контейнера в котором производится проверка :required полей
	// data -- объект в который будут добавлены required поля с именем которое хранится в поле data-<field_name>
	// возвращает true если все поля заполнены, незаполненым полям задает класс error-input, который удаляется 
	// при получения фокуса 
	function CheckForm(selector, data) {
		var	flag = true;
		$(`${selector} :required`).each(function(){
			var value = $.trim($(this).val()),
				dataField = $(this).data("name");
			
			if (value == 0 || value == "" || value == undefined) {
				$(this).addClass("error-input").one("focus", function(){ 
					$(this).removeClass("error-input")
				});
				flag = false;
			} else {
				data[dataField] = value;
			}
		});							
		return flag;
	};

	// Editable lists
	// 3 Загружает список континентов в контейнер "continent_list" Закладка Continent
	function LoadContinentList(selector) {
		$.get("request.php?action=get_continent_list")
				.done(function(inputData) {
					var data = JSON.parse(inputData),
					continents = "";

					for (let ind = 0; ind < data.length; ind++) {
						continents +=  `<li data-id="${data[ind].id}"><div class="list-name">${data[ind].name}</div>
							<a href="" class="glyphicon glyphicon-pencil"></a>
							<a href="" class="glyphicon glyphicon-remove"></a></li>`;
				}
				$(selector).html(continents);
			})
			.fail(function(e) {
				ShowError("Error: " + e.status + " " + e.statusText);
		});	
	}
	
	// 2 Загружает список стран в контейнер "country_list", Закладка Country в случае если их нет, говорит что список пуст
	function LoadContryList(id, selector) {
		var	data = {
				action: "get_continent_country",
				id: id
			}
		$.get("request.php", data)
			.done(function(inputData) {
				var data = JSON.parse(inputData),
					countries = "";

				for (let ind = 0; ind < data.length; ind++) {
					countries +=  `<li data-id="${data[ind].id}"><div class="list-name">${data[ind].name}</div>
						<a href="" class="glyphicon glyphicon-pencil"></a>
						<a href="" class="glyphicon glyphicon-remove"></a></li>`;
				}
				$(selector).find("li").remove();
				$(selector).html(countries);
				if ($(`${selector} li`).length == 0) {
					$(selector).html("<li>No countries existing on this continent</li>");
				}
			})
			.fail(function(e) {
				ShowError("Error: " + e.status + " " + e.statusText);
			}
		);	
	}
	
	// 1 Загружает список городов в контейнер "city_list", Закладкав City случае если их нет, говорит что список пуст
	function LoadCityList(id, selector) {
		var data = {
				action: "get_country_city",
				id: id
		}
		$.get("request.php", data)
			.done(function(inputData) {
				var data = JSON.parse(inputData),
					cities = "";
				for (let ind = 0; ind < data.length; ind++) {
					cities +=  `<li data-id="${data[ind].id}"><div class="list-name">${data[ind].name}</div>
						<a href="" class="glyphicon glyphicon-pencil"></a>
						<a href="" class="glyphicon glyphicon-remove"></a></li>`;
				}
				$(selector).find("li").remove();
				$(selector).append(cities);
				if ($(`${selector} li`).length == 0) {
					$(selector).html("<li>No cities existing in this country</li>");
				}
			})
			.fail(function(e) {
				ShowError("Error: " + e.status + " " + e.statusText);
			}
		);	
	}

	// Загружает список городов указанной страны в контейнер container, в случае если их нет, говорит что список пуст
	function LoadCitySelect(id, selector) {
		var data = {
				action: "get_country_city",
				id: id
		}

		$.get("request.php", data)
			.done(function(inputData) {
				var data = JSON.parse(inputData),
					cities = "";
				if (data.length > 0) {
					for (let ind = 0; ind < data.length; ind++) {
						cities +=  `<option data-id="${data[ind].id}">${data[ind].name}</option>`;
					}
					$(selector).html("<option>Choose capital</option>");
					$(selector).append(cities).attr("disabled", false);
				} else {
					$(selector).html("<option>No cities</option>").attr("disabled", "disabled");
				}
			})
			.fail(function(e) {
				ShowError("Error: " + e.status + " " + e.statusText);
			}
		);	
	}

	// Загружает список <option> в <select>, если пользователь не выбрал поля "continent" или "country" - просит их выбрать.
	//Если стран нет - выводит сообщение что континет пуст. Пока  континент не выбран, список стран закрыт. 
	// Загружает список стран указанного
	function Tab1LoadContry(id, selector) {
		LoadContryToSelect(id, selector);
		$("#city_list .edited-list").html(`<li><div class="list-name"> Please, choose your continent and country first!</div></li>`);
	}
	
	// Загружает список <option> стран в диалоги, если пользователь не выбрал поля "continent" или "country" - просит их выбрать.
	// Если стран нет - выводит сообщение что континет пуст. Пока  континент не выбран, список стран закрыт. 
	// список стран в окне создания города 1 закладка
	function LoadContryToSelect(id, selector, selectedId) {
		var requestData = {
				action: "get_continent_country",
				id: id
			}

		$.get("request.php", requestData)
			.done(function(inputData) {
				var data = JSON.parse(inputData),
					countries = "";
				if (data.length > 0) {
					for (let ind = 0; ind < data.length; ind++) {
						let sel = '';
						if(selectedId !== undefined && selectedId == data[ind].id) sel = "selected";
							countries +=  `<option value="${data[ind].id}">${data[ind].name}</option>`;
					}
					//console.log(countries)
					$(selector).html("<option>Select country</option>").attr("disabled", "disabled");
					$(selector).append(countries).attr("disabled", false);
				} else {
					$(selector).html("<option>No countries</option>").attr("disabled", "disabled");
				}
			})
			.fail(function(e) {
				ShowError("Error: " + e.status + " " + e.statusText);
			}
		);	
	}
	//  Загружает список континентов в SELECT c селектором selector, ставя option с id = selectedId выделенным
	function LoadContinentToSelect(selector, selectedId) {
		$.get("request.php", {action: "get_continent_list"})
			.done(function(inputData) {
				var data = JSON.parse(inputData),
					options = "";

				for (let ind = 0; ind < data.length; ind++) {
					let sel = '';
					if(selectedId !== undefined && selectedId == data[ind].id) sel = "selected";
					options +=  `<option value="${data[ind].id}" ${sel}>${data[ind].name}</option>`;
				}
				$(selector).find("option:not(:first-child)").remove();
				$(selector).append(options);
			})
			.fail(function(e) {
				ShowError("Error: " + e.status + " " + e.statusText);
			}
		);
	}
	
	// Dialogs area -----------------------------------------
	//Create City Dialog 
	$("#dial_city_edit").dialog({
		autoOpen: false,
		modal: true,
		resazible: false,
		minWidth: 300,
		buttons: [
			{
				text: "Save",
				click: function(){
					var	data = {
							action: "create_city",
						},
						flag = CheckForm("#dial_city_edit", data);
					// проверка флага
					if (!flag){
						ShowError("Проверьте заполненность формы, пустые отмечены красным цветом");
						return false;
					}

					$.ajax({
						url:"request.php",
						data: data,
						type: "post",
						dataType: "JSON",
						success: function(res){
							if (res.result) {
								// insert refresh city list for cointry first_tab_country_list (done)
								LoadCityList($("#first_tab_country_list").val(), "#city_list .edited-list");
								ShowMessage("the action worked successfully");
								$("#dial_city_edit").dialog("close");
								ClearInp("#dial_city_edit");

							} else if(res.error) {
								ShowError("Error: " + res.error);
							}else {
								ShowError("Unknown error in action create city");
							}							
						},
						error: function(e){
							ShowError("Error: " + e.status + " " + e.statusText);
						}
					});
				}
			}, 
			{
				text: "Cancel",
				click: function(){
					$(this).dialog("close");
					ClearInp("#dial_city_edit");

				}
			}
		]
	});
	
	// Create Country dialog
	$("#dial_country_edit").dialog({
		autoOpen: false,
		modal: true,
		resazible: false,
		minWidth: 300,
		buttons: [
			{
				text: "Save",
				click: function(){
					var data = {
							action: "create_country"
						},
						flag = CheckForm("#dial_country_edit", data);
					// проверка флага
					if (!flag){
						ShowError("Проверьте заполненность формы, пустые отмечены красным цветом");
						return false;
					}

					$.ajax({
						url:"request.php",
						data: data,
						type: "post",
						dataType: "JSON",
						success: function(res){
							if (res.result) {
								LoadContryList($("#first_tab_country_list").val(), "#city_list .edited-list");
								LoadContryList($("#dialogContinentList").val(), "#country_list .edited-list");
								ShowMessage("the action worked successfully");
								$("#dial_country_edit").dialog("close");
								ClearInp("#dial_country_edit");

							} else if(res.error) {
								ShowError("Error: " + res.error);
							}else {
								ShowError("Unknown error in action create country");
							}
						},
						error: function(e){
							ShowError("Error: " + e.status + " " + e.statusText);
						}
					});
				}
			}, 
			{
				text: "Cancel",
				click: function(){
					$(this).dialog("close");
					ClearInp("#dial_country_edit");

				}
			}
		]
	});

	// Create Continent dialog
	$("#dial_countinent_edit").dialog({
		autoOpen: false,
		modal: true,
		resazible: false,
		minWidth: 300,
		buttons: [
			{
				text: "Save",
				click: function(){
					var data = {
							action: "create_continent"
						},
						flag = CheckForm("#dial_countinent_edit", data);
					// проверка флага
					if (!flag){
						ShowError("Проверьте заполненность формы, пустые отмечены красным цветом");
						return false;
					}
					
					$.ajax({
						url:"request.php",
						data: data,
						type: "post",
						dataType: "JSON",
						success: function(res){
							if (res.result) {
								LoadContinentList("#continent_list .edited-list");
								ShowMessage("the action worked successfully");
								$("#dial_countinent_edit").dialog("close");
							} else if(res.error) {
								ShowError("Error: " + res.error);
							}else {
								ShowError("Unknown error in action create continent");
							}
						},
						error: function(e){
							ShowError("Error: " + e.status + " " + e.statusText);
						}
					});
					ClearInp("#dial_countinent_edit");

				}
			}, 
			{
				text: "Cancel",
				click: function(){
					$(this).dialog("close");
					ClearInp("#dial_countinent_edit");

				}
			}
		]
	});
	
	// Message dialog
	$("#dial_message").dialog({
		autoOpen: false,
		modal: true,
		buttons: {
			"Ok": function(){
				$( this ).dialog( "close" );
			}
		}
	});
	
	//Dialog continent redesing 3
	$("#dialog_continent_redesing").dialog({
		autoOpen: false,
		modal: true,
		resazible: false,
		minWidth: 300,
		buttons: [
			{
				text: "Save",
				click: function(){
					console.log(currData);
					var flag = CheckForm("#dialog_continent_redesing", currData);
					// проверка флага
					if (!flag){
						ShowError("Проверьте заполненность формы, пустые отмечены красным цветом");
						return false;
					}
					
					$.ajax({
						url:"request.php",
						data: currData,
						type: "post",
						dataType: "JSON",
						success: function(res){
							if (res.result) {
								LoadContinentToSelect("#сontinent_select_field, #first_tab_continent_list, #dial_redesing_continent_list, #sec_tab_continent_list");
								ChangeText(selectedLi, "#edit_cont_name");
								ClearInp("#dialog_continent_redesing");
								ShowMessage("the action worked successfully");
							} else if(res.error) {
								ShowError("Error: " + res.error);
							}else {
								ShowError("Unknown error in action redesing continent");
							}
							$("#dialog_continent_redesing").dialog("close");
						},
						error: function(e){
							ShowError("Error: " + e.status + " " + e.statusText);
						}
					});
				}
			}, 
			{
				text: "Cancel",
				click: function(){
					$(this).dialog("close");
					ClearInp("#dialog_continent_redesing");

				}
			}
		]
	});
	
	//Dialog country redesing 2
	$("#dialog_country_redesing").dialog({
		autoOpen: false,
		modal: true,
		resazible: false,
		minWidth: 300,
		buttons: [
			{
				text: "Save",
				click: function(){
					var flag = CheckForm("#dialog_country_redesing", currData),
						curOpt = $("#redisign_country_city option:selected").data();
					currData.capital_id = curOpt.id + "";
					console.log(currData);

					// проверка флага
					if (!flag){
						ShowError("Проверьте заполненность формы, пустые отмечены красным цветом");
						return false;
					}
					
					$.ajax({
						url:"request.php",
						data: currData,
						type: "post",
						dataType: "JSON",
						success: function(res){
							if (res.result) {
								//LoadContryList();
								ChangeText(selectedLi, "#name_container");
								$("#dialog_country_redesing").dialog("close");
								ClearInp("#dialog_country_redesing");
								ShowMessage("the action worked successfully");
							} else if(res.error) {
								ShowError("Error: " + res.error);
							}else {
								ShowError("Unknown error in action redesing country");
							}
						},
						error: function(e){
							ShowError("Error: " + e.status + " " + e.statusText);
						}
					});
				}
			}, 
			{
				text: "Cancel",
				click: function(){
					$(this).dialog("close");
					ClearInp("#dialog_country_redesing");

				}
			}
		]
	});
	
	// Dialog city redesing 1
	$("#dialog_city_redesing").dialog({
		autoOpen: false,
		modal: true,
		resazible: false,
		minWidth: 300,
		buttons: [
			{
				text: "Save",
				click: function(){
					//console.log(currData);
					var flag = CheckForm("#dialog_city_redesing", currData);
					// проверка флага
					if (!flag){
						ShowError("Проверьте заполненность формы, пустые отмечены красным цветом");
						return false;
					}
					
					$.ajax({
						url:"request.php",
						data: currData,
						type: "post",
						dataType: "JSON",
						success: function(res){
							if (res.result) {
								ShowMessage("the action worked successfully");
							} else if(res.error) {
								ShowError("Error: " + res.error);
							}else {
								ShowError("Unknown error in action redesing city");
							}
							$("#dialog_city_redesing").dialog("close");
						},
						error: function(e){
							ShowError("Error: " + e.status + " " + e.statusText);
						}
					});
					ClearInp("#dialog_city_redesing");
				}
			}, 
			{
				text: "Cancel",
				click: function(){
					$(this).dialog("close");
					ClearInp("#dialog_city_redesing");

				}
			}
		]
	});
	
	// Dialogs openers
	// city_edit
	$("#create_city").click(function(){
		$("#dial_city_edit").dialog("open");
	});

	// country_edit
	$("#create_country").click(function(){
		$("#dial_country_edit").dialog("open");
	});

	// countinent_edit
	$("#create_continent").click(function(){
		$("#dial_countinent_edit").dialog("open");
	});

	// Delete continent
	$(document.body).on("click", "#continent_list .glyphicon-remove", function(){
		if (!confirm("Are you realy whant to delite this?")) return false;

		var listItem = $(this).closest("li"),
			data = {
				action: "delete_continent",
				id: listItem.attr("data-id")
			};

		$.ajax({
			url:"request.php",
			data: data,
			type: "post",
			dataType: "JSON",
			success: function(res){
				if(!res.error){
					listItem.remove();
				}else{
					ShowError(res.error);
				}
			},
			error: function(e){
				ShowError("Error: " + e.status + " " + e.statusText);
			}
		});
		return false;
	});

	// Second tab country list
	$(document.body).on("change", "#sec_tab_continent_list", function(){
		var	id = $(this).find("option:selected").val();
		LoadContryList(id, "#country_list .edited-list");
		return false;
	});

	// First dialog country list
	$(document.body).on("change", "#сontinent_select_field", function(){
		var id = $(this).find("option:selected").val();

		LoadContryToSelect(id, "#country_select_field");
		return false;
	});
	//1 tab  dialog redisign country list
	$(document.body).on("change", "#city_redesing_continent_list", function(){
		var id = $(this).find("option:selected").val();

		LoadContryToSelect(id, "#country_select");
		return false;
	});
	
	// First tab country list
	$(document.body).on("change", "#first_tab_continent_list", function(){
		var id = $(this).find("option:selected").val();

		Tab1LoadContry(id, "#first_tab_country_list");
		return false;
	});
	
	// First tab city list
	$(document.body).on("change", "#first_tab_country_list", function(){
		id = $(this).find("option:selected").val();
		
		LoadCityList(id, "#city_list .edited-list");
		return false;
	});

	// Delete country from continent
	$(document.body).on("click", "#country_list .glyphicon-remove", function(){
		if (!confirm("Are you realy whant to delite this?")) return false;

		var listItem = $(this).closest("li"),
			data = {
				action: "delete_country",
				id: listItem.attr("data-id")
			};

		$.ajax({
			url: "request.php",
			data: data,
			type: "post",
			dataType: "JSON",
			success: function(res){
				if (!res.error) {
					listItem.remove();
				} else {
					ShowError(res.error);
				}
			},
			error: function(e){
				ShowError("Error: " + e.status + " " + e.statusText);
			}
		});	
		return false;
	});

	// Delete city from country
	$(document.body).on("click", "#city_list .glyphicon-remove", function() {
		if (!confirm("Are you realy whant to delite this?")) return false;

		var listItem = $(this).closest("li"),
			data = {
				action: "delete_city",
				id: listItem.attr("data-id")
			};
			
		$.ajax({
			url: "request.php",
			data: data,
			type: "post",
			dataType: "JSON",
			success: function(res){
				if (!res.error) {
					listItem.remove();
				} else {
					ShowError(res.error);
				}
			},
			error: function(e){
				ShowError("Error: " + e.status + " " + e.statusText);
			}
		});	
		return false;
	});
	
	// Continent redesigning 3
	$(document.body).on("click","#continent_list .glyphicon-pencil", function(){
		selectedLi = $(this).closest("li");
		currData = {
			action: "edit_continent",
			id: selectedLi.attr("data-id")
		}	

		$("#dialog_continent_redesing").dialog("open");
		$("#edit_cont_name").val(selectedLi.find(".list-name").text());
		return false;
	});

	// Country redesigning 2
	$(document.body).on("click","#country_list .glyphicon-pencil", function(){
		selectedLi = $(this).closest("li"),
			id = selectedLi.attr("data-id");
			currData = {
				action: "edit_country",
				id: selectedLi.attr("data-id"),
			},
			countryGetter = {
				action: "get_country",
				id: id
			};

		$.get("request.php", countryGetter)
			.done(function(inputData) {
				var data = JSON.parse(inputData),
					continentGetter = {
						action: "get_continent",
						id: data[0].parent_id
					}
				$("#country_pop").val(data[0].population); // население страны
				$("#country_square").val(data[0].square); // площадь страны


				$.get("request.php", continentGetter)
					.done(function(inputData) {
						var data = JSON.parse(inputData),
							continentId = data[0].id;

						$("#redesing_continent_list").val(continentId); // имя континента 
					})
					.fail(function(e) {
						ShowError("Error: " + e.status + " " + e.statusText);
					}
				);	
			})
			.fail(function(e) {
				ShowError("Error: " + e.status + " " + e.statusText);
			}
		);	
		$("#dialog_country_redesing").dialog("open");
		$("#name_container").val(selectedLi.text());
		LoadCitySelect(id, "#redisign_country_city");
		return false;
	});

	// City redesigning 1
	$(document.body).on("click","#city_list .glyphicon-pencil", function(){
		var listItem = $(this).closest("li");

		currData = {
			action: "edit_city",
			id: listItem.attr("data-id")
		},
		CityData = {
			action: "get_city",
			id: listItem.attr("data-id")
		};
		$.get("request.php", CityData)
			.done(function(inputData) {
				var data = JSON.parse(inputData),
					countryId = data[0].parent_id, // id страны
					currPop = data[0].population, // Наседение (текущего) города
					countryData = {
						action: "get_country",
						id: countryId
					}

				$("#city_population").val(currPop); // запихнули население в input

				$.get("request.php", countryData)
					.done(function(inputData) {
						var data = JSON.parse(inputData),
							continentId = data[0].parent_id; // id континента страны 
						$("#city_redesing_continent_list").val(continentId); // выбрали нужную страну
						
					})
					.fail(function(e) {
						ShowError("Error: " + e.status + " " + e.statusText);
					}
				);	
			})
			.fail(function(e) {
				ShowError("Error: " + e.status + " " + e.statusText);
			}
		);	
		$("#dialog_city_redesing").dialog("open");
		
		// cityId, cityName
		// Read City Properties -> population, parenId (countryId) (done)
		// Read Country with parentId -> (continentId) (done)
		// checked curr continentId (done)
		// ReadCountry() and select currCountry 
		$("#redisign_city_name").val(listItem.text());
		// set population (done)



		return false;
	});

	// init tabs
	$("#main_tab").tabs({active: 0});
	$("#create_city, #create_country, #create_continent").button();
	$("#first_tab_country_list").html("<option>No countries</option>").attr("disabled", "disabled");
	// Init section
	$("#country_select_field").html("<option>No countries</option>").attr("disabled", "disabled");
	LoadContinentToSelect("#сontinent_select_field, #first_tab_continent_list, #redesing_continent_list, #city_redesing_continent_list, #dial_redesing_continent_list, #sec_tab_continent_list, #dialogContinentList");
	LoadContinentList("#continent_list .edited-list");
});