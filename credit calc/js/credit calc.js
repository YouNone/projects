$(function(){

	// секция инициализации переменных
		var	minCredSumm = 2000000,
			maxCredSumm = 4000000,
			credStep = 1000,
			monthStep = 1,
			monthMin = 1,
			monthMax = 240,
			sumState,
			curFunc = SolveClassicalCrediting,
			tableIsHide = false,
			MINPERS = 0,
			MAXPERS = 0;
	
		var creditData = {
			ipot: {
				minCredSumm: 2000000,
				maxCredSumm: 4000000,
				monthMin: 12,
				monthMax: 240,
				pers: 12, 								// кредит ставка
				minCred: 0.3,
				maxCred: 0.9,						
				startPayMin: 0.3 * 2000000,				// минимальный начальный взнос в %
				startPayMax: 0.9 * 2000000,				// максимальный начальный взнос в %
				
			},
			avto: {
				minCredSumm: 100000,
				maxCredSumm: 2000000,
				monthMin: 5,
				monthMax: 60,
				pers: 18,								// кредит ставка
				minCred: 0.2,
				maxCred: 0.8,
				startPayMin: 0.2 * 100000,				// минимальный начальный взнос в %
				startPayMax: 0.8 * 100000				// максимальный начальный взнос в %
			},
			potr: {
				minCredSumm: 5000,
				maxCredSumm: 1000000,
				monthMin: 1,
				monthMax: 12,
				minCred: 0,
				maxCred: 0,
				pers: 25,								// кредит ставка
				startPayMin: 0,							// минимальный начальный взнос в %
				startPayMax: 0							// максимальный начальный взнос в %
			}
		}
	
		var creditState = {
				typeOfCred: '',
				typeOfRepaiment: '',
				minCredSumm: 0,
				maxCredSumm: 0,
				summ: 0,
				firstPay: 0,
				termOfCred: 0,
				pers: 0,
				minCred: 0,
				maxCred: 0,
				currPayMin: 0,
				currPayMax: 0,
		}
	
	// инициализация слайдеров	
		var sliderSumm = new Slider('#summ', {
			formatter: function(value) {
				$("#summ_inp").val(value);
			}
		});
		var sliderFirstPay = new Slider('#first_payment', {
			formatter: function(value) {
				$("#first_payment_inp").val(value);
			}
		});
		var sliderTermOfCred = new Slider('#term_of_credit', {
			formatter: function(value) {
				$("#term_of_credit_inp").val(value);
			}
		});
	
	// производит перерасчет кредита при значений параметров калькулятора
		function Recaulc() {
			var credSumm = +$("#summ_inp").val(),
				termOfCred = +$("#term_of_credit_inp").val(),
				firstPay = +$("#first_payment_inp").val(),
				persent = +creditData[$("#type_credit_select").val()].pers;
	
			creditState.typeOfCred = $("#type_credit_select").val();
			sumState = curFunc(credSumm, termOfCred, persent, firstPay);
			if (tableIsHide == true) {
				$("#results_container").css("display", "none");
				tableIsHide = false;
			}
			$("#summ_credit").text(sumState.summ);
			$("#loan_amount").text(sumState.loan);
			$("#Interest_payments").text(sumState.persent);
		}
	
	// вставляет данные в таблицу результатов
		function DataEditor(state) {
			var tbl = '';			
			
			for (var i = 0; i < state.payList.length; i++){
				tbl += '<tr><td>'+ (i + 1) + '</td>';
				tbl += '<td>'+ state.payList[i].pay + '</td>';
				tbl += '<td>'+ state.payList[i].persPay + '</td></tr>';
			}
			$('#result_table tbody').append(tbl);
			$('#result_table tfoot td:nth-child(2)').text(state.summ);
			$('#result_table tfoot td:nth-child(3)').text(state.persent);
	
		}
	
	// Считает аннуитетный платеж
		function SolveAnnuityCrediting(credSumm, termOfCred, persent, firstPay) {
			// Формула аннуитетного платежа monthPay = koef * credSumm
			// monthPay — размер ежемесячного платёжа по аннуитетной схеме
			// koef — коэффициент	
			// credSumm — сумма кредита
			// koef = persent * Math.pow((1 + persent), termOfCred) / Math.pow((1 + persent), termOfCred) - 1
			// persent — процентная ставка по кредиту за месяц (годовая ставка в формате дроби / 12)
			// termOfCred — количество месяцев, на которые разделен кредит
	
			var annuityRes = [],
				koef = 0,
				persent = persent / 100 / 12,
				persentPay = 0,
				totalSumm = 0,
				totalCred = 0,
				loanAmount = 0, // Сумма займа
				monthPay = 0;
	
			loanAmount = credSumm - firstPay;	
			koef = persent * Math.pow((1 + persent), termOfCred) / (Math.pow((1 + persent), termOfCred) - 1);	
			monthPay = Math.floor(koef * loanAmount); 						// Формула аннуитетного платежа c вычитанием стартового платежа
			persentPay = Math.round(monthPay - loanAmount / termOfCred); 	// тело платежа
			
			
			for (let ind = 1; ind <= termOfCred; ind++) {
				totalSumm += monthPay;
				totalCred += persentPay;
				annuityRes.push({pay: monthPay, persPay: persentPay});
			}
			return {
				summ: totalSumm,
				persent: totalCred,
				loan: loanAmount,
				payList: annuityRes
			}
		}
	
	// Считает дифференцированный платеж
		function SolveClassicalCrediting(credSumm, termOfCred, persent, firstPay) {
			// Формула дифференцированного платежа D = B + P
			// D – размер ежемесячного платежа по дифференцированной схеме
			// b – основной ежемесячный платеж по телу кредита
			// p – начисленные на остаток проценты
			// b = credSumm / termOfCred (расчет суммы ежемесячных выплат по телу кредита)
			// credSumm – размер кредита
			// termOfCred – количество месяцев, на которые взят кредит
			// p = (credSumm – ( b * pasedTerm )) * P / 12 (расчет процентов для каждого месяца с учетом погашения тела кредита)
			// pasedTerm – количество прошедших месяцев
			// P – годовая процентная ставка по кредитy (переведенная в дробь, например, 0,32 при ставке 32%)
	
			var classicalRes = [],
				monthPay,
				pasedTerm, // количество прошедших месяцев
				persentPay,
				totalSumm = 0,
				totalCred = 0,
				loanAmount = 0; //Сумма займа
	
			loanAmount = credSumm - firstPay;
			credSumm = credSumm - firstPay;									// вычитаем начальный платеж
			b = credSumm / termOfCred;  									// расчет суммы ежемесячных выплат по телу кредита
			//p = (credSumm - (b * pasedTerm)) * (persent / 100) / 12;		// расчет процентов для каждого месяца с учетом погашения тела кредита
			//monthPay = b + p;
			pasedTerm = termOfCred;
	
			for (let i = 1; i <= termOfCred; i++) {
				pasedTerm--;
				p = (credSumm - (b * pasedTerm)) * (persent / 100) / 12;
				monthPay = Math.round(b + p);
				persentPay = Math.round(monthPay - (credSumm - firstPay) / termOfCred);
	
				classicalRes.unshift({pay: monthPay, persPay: persentPay});
				totalSumm += monthPay;
				totalCred += persentPay;
			}
			//console.log("тело платежа: ", persentPay, "размер ежемесячного платёжа по аннуитетy: ", monthPay);
	
			return {
				summ: totalSumm,
				persent: totalCred,
				loan: loanAmount,
				payList: classicalRes
			};
		}
		
	
	// меняет диапазон минимальных и максимальных пределов кредита
		function ChangeMinMaxVal(obj, minVal, maxVlal) {
			obj.options.min = minVal;
			obj.options.max = maxVlal;
		}
	
	// проверяет входящие значения в инпут, если они больше/меньше порогов устанавливает мин/макс значения
		function CheckInput(selector, MinObjField, MaxobjField) {
			if ($(selector).val() < creditData[creditState.typeOfCred].MinObjField) {
				$(selector).val() = creditData[creditState.typeOfCred].MinObjField
			}
			if ($(selector).val() > creditData[creditState.typeOfCred].MaxobjField) {
				$(selector).val() = creditData[creditState.typeOfCred].MaxobjField
			}
		}
		
	// считает проценты от числа и показывает их 
		function PersentStr(currSum, totalSumm) {
			//console.log( currSum, totalSumm);
			return currSum * 100 / totalSumm;
		}
	
	// меняет текст селектора на заданный второй параметр
		function SetText(selector, value) {
			$(selector).text(value);
		}
	
	// изходя из данных state выбирает какой тип кредита будет на ползунках
		function SetStartState(state){
			SetText("#sum_min", creditData[state].minCredSumm);
			SetText("#sum_max", creditData[state].maxCredSumm);
			SetText("#credit_min", creditData[state].monthMin);
			SetText("#credit_max", creditData[state].monthMax);
			SetText("#first_payment_min", creditData[state].startPayMin);
			SetText("#first_payment_max", creditData[state].startPayMax);
			$("#type_of_repayment_persent").val(creditData[state].pers + "%");
			ChangeMinMaxVal(sliderSumm, creditData[state].minCredSumm, creditData[state].maxCredSumm);
			ChangeMinMaxVal(sliderTermOfCred, creditData[state].monthMin, creditData[state].monthMax);
			ChangeMinMaxVal(sliderFirstPay, creditData[state].startPayMin, creditData[state].startPayMax);
			sliderSumm.setValue(credStep);
			sliderFirstPay.setValue(creditData[state].startPayMin);
			sliderTermOfCred.setValue(creditData[state].monthMin);
	
			//creditState.summ = +$("#summ_inp").val();
			creditState.firstPay = $("#first_payment_inp").val();
			creditState.typeOfCred = $("#type_credit_select").val();
			creditState.termOfCred = $("#term_of_credit_inp").val();
			creditState.minCred = creditData[state].minCred;
			creditState.maxCred = creditData[state].maxCred;
			creditState.minCredSumm = creditData[state].minCredSumm;
			creditState.maxCredSumm = creditData[state].maxCredSumm;
			creditState.currPayMin = creditData[state].startPayMin;
			creditState.currPayMax = creditData[state].startPayMax;
	
			//sliderFirstPay.setValue(credStep);
			tableIsHide = true;
			// if (creditState.currPayMin !== creditState.currPayMax) {
			// 	if (creditState.typeOfCred == "ipot") {	
			// 		MINPERS = PersentStr(creditState.currPayMin, creditState.summ).toFixed(2)
			// 		//?
			// 		SetText("#first_payment_min", creditState.summ * creditState.minCred + " " + MINPERS);
			// 		ChangeMinMaxVal(sliderFirstPay, creditState.summ * creditState.minCred, creditState.summ * creditState.maxCred);
			// 		creditState.firstPay = creditState.summ * creditState.minCred;
			// 		console.log(creditState.firstPay);
	
			// 	} else {
			// 		MINPERS = PersentStr(creditState.currPayMin, creditState.summ).toFixed(2)
			// 		//?
			// 		creditState.firstPay = creditState.summ * creditState.minCred;
			// 		console.log(creditState.firstPay);
			// 		SetText("#first_payment_min", creditState.summ * creditState.minCred + " " + MINPERS);
			// 	}
			// 	$("#summ_credit").text($("#summ_inp").val());
			// }
		
			if (creditData[state].startPayMin == creditData[state].startPayMax) {
				SetText("#first_payment_min", "0");
				SetText("#first_payment_max", "0");
				$("#first_payment_inp").prop("disabled", true);
				sliderFirstPay.disable();
			} else { 
				$("#first_payment_inp").prop("disabled", false);
				sliderFirstPay.enable();
			}
		}
	
	// кнопка открывающая таблицу результатов
	
		$("#schedule_button").click(function(){
			tableIsHide = false;
	
			$("#result_table tbody").text("");
			$("#results_container").css("display", "block");
			DataEditor(sumState);
			Recaulc();
		});
		
	// событие отлова типа кредита
		$("#type_credit_select").change(function(){
			tableIsHide = true;
			var currOpt = $("#type_credit_select option:selected").val();
			$("#summ_credit").text("0");
			SetStartState(currOpt);
			Recaulc();
		});
	
	// события отображающие value ползунков в value инпутов
		$("#summ_inp").blur(function(){
			CheckInput("#summ_inp", "minCred", "maxCred")
	
			tableIsHide = true;
			sliderSumm.setValue($(this).val());
			sliderFirstPay.setValue(credStep);
			$("#summ_credit").text($(this).val());
			Recaulc();
		});
	
		$("#first_payment_inp").blur(function(){
			CheckInput("#first_payment_inp", "minCred", "maxCred")
			tableIsHide = true;
			sliderFirstPay.setValue($(this).val());
			Recaulc();
		});
	
		$("#term_of_credit_inp").blur(function(){
			CheckInput("#term_of_credit_inp", "minCred", "maxCred")
			tableIsHide = true;
			sliderTermOfCred.setValue($(this).val());
			Recaulc();
		});
	
		$("#first_payment").change(function(){
			tableIsHide = true;
			Recaulc();
		});
		$("#term_of_credit").change(function(){
			tableIsHide = true;
			Recaulc();
		});
	
	// выбирает нужный верхний и нижний барьер начального взноса и устанавливает его
		$("#summ").change(function(){
			tableIsHide = true;
			creditState.summ = +$("#summ_inp").val();
			creditState.firstPay = +$("#first_payment_inp").val();
			sliderFirstPay.setValue(credStep);

			if (creditState.currPayMin !== creditState.currPayMax) {

				if (creditState.typeOfCred == "ipot") {	
					MINPERS = PersentStr(creditState.firstPay, creditState.summ).toFixed(2);
					console.log(MINPERS);
					SetText("#first_payment_min", creditState.summ * creditState.minCred + " " + MINPERS);
					SetText("#first_payment_max", creditState.summ * creditState.maxCred);
					ChangeMinMaxVal(sliderFirstPay, creditState.summ * creditState.minCred, creditState.summ * creditState.maxCred);
				} else {
					console.log(MINPERS);
					MINPERS = PersentStr(creditState.firstPay, creditState.summ).toFixed(2);
					SetText("#first_payment_min", creditState.summ * creditState.minCred + " " + MINPERS);
					SetText("#first_payment_max", creditState.summ * creditState.maxCred);
					ChangeMinMaxVal(sliderFirstPay, creditState.summ * creditState.minCred, creditState.summ * creditState.maxCred);
	
				}
				$("#summ_credit").text($("#summ_inp").val());
				$("#first_payment_inp").val(creditState.summ * creditState.minCred);
			}
			Recaulc();
		});
	
	// отлов события типа погашения кредита и вызов функии расчета
		$("#type_of_repayment").change(function(){
			tableIsHide = true;
			curFunc = $("#type_of_repayment").val() == "classical" ? SolveClassicalCrediting : SolveAnnuityCrediting;
			Recaulc();
		});
	
		// init start state
		(function(){
			var currOpt = $("#type_credit_select").val();
			curFunc = $("#type_of_repayment").val() == "classical" ? SolveClassicalCrediting : SolveAnnuityCrediting;
			SetStartState(currOpt);
			SetText("#sum_min", minCredSumm);
			SetText("#sum_max", maxCredSumm);
			Recaulc();
		})();
	});
	