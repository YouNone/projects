-- Вывести сумму заказов всех неактивных пользователей
select sum(p.amount)
from customer as c
join payment as p on p.customer_id = c.customer_id
where c.active = 0
