-- Вывести 5 самых популярных актёров
select ac.actor_id, ac.last_name, ac.first_name, count(fa.actor_id) as act_f
from actor as ac
join film_actor as fa on fa.actor_id = ac.actor_id
group by ac.actor_id
order by act_f desc
limit 5