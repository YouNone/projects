-- select top 10 most active customers and sum of their orders, and count of viewed films
--  count() sum()
select 
	c.customer_id,
	c.first_name, 
    c.last_name,
    sum(p.amount) as total_sum,
    count(r.customer_id) as rent_cnt
    
from customer as c
join rental as r on r.customer_id = c.customer_id
join payment as p on p.rental_id = r.rental_id
group by r.customer_id
order by rent_cnt desc
limit 10 
-- select top 10 most active customers and sum of their orders, and count of viewed films
--  count() sum()
select 
	c.customer_id,
	c.first_name, 
    c.last_name,
	(
		select sum(p1.amount) 
        from payment as p1 
        join rental as r1 on r1.rental_id = p1.rental_id
        where p1.customer_id = c.customer_id
	) as total_pay,
    count(r.customer_id) as rent_cnt
    
from customer as c
join rental as r on r.customer_id = c.customer_id
join payment as p on p.rental_id = r.rental_id
group by r.customer_id
order by rent_cnt desc
limit 10 