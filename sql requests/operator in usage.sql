-- select order of customers, and info about them(cust), 
-- from cities with id 300, 200, 280, 290 (order sum and date, film name, cust. name, adress)
select  a.city_id, p.amount, r.rental_date, f.title, c.first_name, c.last_name, a.district, a.address
from rental as r
join payment as p on p.rental_id = r.rental_id
join inventory as i on i.inventory_id = r.inventory_id
join film as f on f.film_id = i.film_id
join customer as c on c.customer_id = r.customer_id
join address as a on a.address_id = c.address_id
join city as ci on ci.city_id = a.city_id
where a.city_id in(200, 280, 290, 300)