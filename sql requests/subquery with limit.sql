-- выбрать все фильмы которые заказывали первые 10 клиентов из списка заказщиков,
-- отсортированых в обратном порядке (выборка клинтов вложеный запрос)
select distinct f.film_id, f.title
from film as f
join inventory as i on i.film_id = f.film_id
join rental as r on r.inventory_id = i.inventory_id
join 
	(
		select 
			c1.customer_id,
			c1.store_id
		from customer as c1
		order by c1.last_name desc
		limit 10 
    ) AS cust ON cust.customer_id =  r.customer_id
