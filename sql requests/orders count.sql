-- select active customers and count of their orders
select c.first_name, c.last_name, count(r.customer_id)
from customer as c
join rental as r on r.customer_id = c.customer_id
where c.active = 1
group by r.customer_id